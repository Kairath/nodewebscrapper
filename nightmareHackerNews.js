const ycombinator = require("./hackerNews");

(async () => {
  await ycombinator.init();
  let articles = await ycombinator.getArticles(100);
  console.log(articles);
  console.log(articles.length);
  await ycombinator.close();
})().catch(e => console.log(e));

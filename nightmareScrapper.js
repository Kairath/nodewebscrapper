const Nightmare = require("nightmare");
const nightmare = Nightmare({
  show: true
  //openDevTools: { mode: "detach" }
});

(async () => {
  //setting viewport
  await nightmare.viewport(1200, 600);
  await nightmare.goto("https://duckduckgo.com");

  await nightmare.type("#search_form_input_homepage", "hmm");
  await nightmare.click("#search_button_homepage");
  await nightmare.wait("#r1-0 a.result__a");
  let result1 = await nightmare.evaluate(() => {
    return document.querySelector("#r1-0 a.result__a").href;
  });

  //scrolling down to page
  let height = await nightmare.evaluate(() => document.body.scrollHeight);
  await nightmare.scrollTo(height, 0); //height(y), width(x)

  //await nightmare.type('selector', 'typing to a selector')
  //let boolVal = await nightmare.exists('selector)
  //let boolVal = await nightmare.visible('selector)
  //let current_url = await nightamer.url();
  //let current_title = await nightmare.title();

  //let cookies = await nightmare.cookies.get(); //all cookies as array

  //await nightmare.end();
})().catch(e => console.log(e));

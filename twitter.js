const puppeteer = require("puppeteer");

const base_url = "https://twitter.com";
const login_url = "https://twitter.com/login";
const username_url = username => `https://twitter.com/${username}`;

let browser = null;
let page = null;

const twitter = {
  initialize: async () => {
    browser = await puppeteer.launch({ headless: false });
    page = await browser.newPage();
    await page.goto(base_url);
  },
  login: async (username, pass) => {
    await page.goto(login_url);
    await page.waitFor(1000);
    await page.waitFor(
      "form[class='t1-form clearfix signin js-signin'] input[name='session[username_or_email]'"
    );
    await page.type(
      "form[class='t1-form clearfix signin js-signin'] input[name='session[username_or_email]'",
      username,
      {
        delay: 100
      }
    );
    await page.type('input[class="js-password-field"', pass, {
      delay: 100
    });
    await page.click("button[type='submit'");
    await page.waitFor("#tweet-box-home-timeline");
    await page.waitFor(1000);
  },
  postTweet: async msg => {
    let url = await page.url();

    if (url != base_url) {
      await page.goto(base_url);
    }
    await page.waitFor("#tweet-box-home-timeline");
    await page.click("#tweet-box-home-timeline");
    await page.waitFor(500);
    await page.keyboard.type(msg, { delay: 100 });
    await page.click(
      "button[class='tweet-action EdgeButton EdgeButton--primary js-tweet-btn'"
    );
  },
  getUser: async username => {
    let url = await page.url();
    if (url != username_url(username)) {
      await page.goto(username_url(username));
    }

    await page.waitFor("h1.ProfileHeaderCard-name > a");
    await page.waitFor(500);

    let {
      fullname,
      description,
      followerCount,
      tweetCount,
      followingsCount,
      likesCount,
      location,
      site,
      joinDate,
      isVerified
    } = await page.evaluate(() => {
      return {
        fullname: document.querySelector("h1.ProfileHeaderCard-name > a")
          ? document.querySelector("h1.ProfileHeaderCard-name > a").innerHTML
          : false,
        description: document.querySelector(
          "p[class='ProfileHeaderCard-bio u-dir']"
        )
          ? document.querySelector("p[class='ProfileHeaderCard-bio u-dir']")
              .innerText
          : false,
        followerCount: document.querySelector(
          "a[data-nav='followers'] span[class='ProfileNav-value']"
        )
          ? document
              .querySelector(
                "a[data-nav='followers'] span[class='ProfileNav-value']"
              )
              .getAttribute("data-count")
          : false,
        tweetCount: document.querySelector(
          "li.ProfileNav-item--tweets span[class='ProfileNav-value']"
        )
          ? document
              .querySelector(
                "li.ProfileNav-item--tweets span[class='ProfileNav-value']"
              )
              .getAttribute("data-count")
          : false,

        followingsCount: document.querySelector(
          "li.ProfileNav-item--following span[class='ProfileNav-value']"
        )
          ? document
              .querySelector(
                "li.ProfileNav-item--following span[class='ProfileNav-value']"
              )
              .getAttribute("data-count")
          : false,
        likesCount: document.querySelector(
          "li.ProfileNav-item--favorites span[class='ProfileNav-value']"
        )
          ? document
              .querySelector(
                "li.ProfileNav-item--favorites span[class='ProfileNav-value']"
              )
              .getAttribute("data-count")
          : false,
        location: document.querySelector("span.ProfileHeaderCard-locationText")
          ? document
              .querySelector("span.ProfileHeaderCard-locationText")
              .innerHTML.trim()
          : false,
        site: document.querySelector("span.ProfileHeaderCard-urlText > a")
          ? document.querySelector("span.ProfileHeaderCard-urlText > a")
              .innerText
          : false,
        joinDate: document.querySelector("span.ProfileHeaderCard-joinDateText")
          ? document.querySelector("span.ProfileHeaderCard-joinDateText")
              .innerText
          : false,
        isVerified: document.querySelector(
          "div.ProfileHeaderCard span.Icon--verified"
        )
          ? true
          : false
      };
    });

    return {
      fullname,
      description,
      followerCount,
      tweetCount,
      followingsCount,
      likesCount,
      location,
      site,
      joinDate,
      isVerified
    };
  },
  getTweets: async (username, count = 10) => {
    let url = await page.url();
    if (url != username_url(username)) {
      await page.goto(username_url(username));
    }

    try {
      await page.waitForSelector(".ProtectedTimeline", { timeout: 1000 });
      return [];
    } catch (err) {
      await page.waitFor("#stream-items-id");
    }

    let tweetsArray = await page.$$("#stream-items-id > .stream-item");
    let tweets = [];
    let lastTweetsArrayLength = tweetsArray.length;

    while (tweetsArray.length < count) {
      await page.evaluate(() => {
        window.scrollTo(0, document.body.scrollHeight);
      });
      await page.waitFor(3000);
      tweetsArray = await page.$$("#stream-items-id > .stream-item");
      if (lastTweetsArrayLength == tweetsArray.length) break;
      lastTweetsArrayLength = tweetsArray.length;
    }

    for (let tweet of tweetsArray) {
      let tweetText = await tweet.$eval(
        ".js-tweet-text-container",
        elem => elem.innerText
      );

      let retweetCount = await tweet.$eval(
        ".stream-item-footer .js-actionRetweet .ProfileTweet-actionCount",
        elem => elem.innerText
      );

      let likeCount = await tweet.$eval(
        ".ProfileTweet-action--favorite  .ProfileTweet-actionCountForPresentation",
        elem => elem.innerText
      );

      //("#stream-items-id > .stream-item .time a").getAttribute("data-original-title")
      let postDate = await tweet.$eval(".tweet-timestamp", elem => {
        return elem.getAttribute("data-original-title")
          ? elem.getAttribute("data-original-title")
          : elem.getAttribute("title");
      });

      let repliesCount = await tweet.$eval(
        ".ProfileTweet-action--reply .ProfileTweet-actionCountForPresentation",
        elem => elem.innerText
      );

      tweets.push({
        tweetText,
        retweetCount,
        likeCount,
        postDate,
        repliesCount
      });
    }
    return tweets.slice(0, count);

    debugger;
  },
  close: async () => {
    await page.close();
  }
};

module.exports = twitter;

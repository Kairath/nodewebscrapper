const axios = require("axios");
const cheerio = require("cheerio");

(async () => {
  let username = "willsmith";
  let base_url = `https://www.instagram.com/${username}/`;

  let response = await axios({
    method: "get",
    url: base_url
  }).catch(e => console.log(e));

  let $ = cheerio.load(response.data);

  let script = $("script[type='text/javascript']")
    .eq(3)
    .html();

  let scriptRegex = /window._sharedData = (.*);/gi.exec(script);
  let instagramData = JSON.parse(scriptRegex[1]);

  let {
    entry_data: {
      ProfilePage: {
        [0]: {
          graphql: { user }
        }
      }
    }
  } = instagramData;

  let timeline_media = user.edge_owner_to_timeline_media.edges;

  let posts = [];

  for (let edge of timeline_media) {
    let { node } = edge;

    posts.push({
      id: node.id,
      shortcode: node.shortcode,
      timestamp: node.taken_at_timestamp,
      likes: node.edge_liked_by.count,
      comments: node.edge_media_to_comment.count,
      video_views: node.video_view_count,
      caption: node.edge_media_to_caption.edges[0].node.text,
      image_url: node.display_url
    });
  }

  let dataz = {
    followers: user.edge_followed_by.count,
    following: user.edge_follow.count,
    uploads: user.edge_owner_to_timeline_media.count,
    full_name: user.full_name,
    picture_url: user.profile_pic_url_hd,
    posts
  };

  debugger;

  console.log(timeline_media);
})();

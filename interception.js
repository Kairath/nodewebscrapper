const puppeteer = require("puppeteer");

(async () => {
  const browser = await puppeteer.launch({
    headless: false,
    ignoreHTTPSErrors: true, //to ignore https/ssl errors
    defaultViewport: { width: 1024, height: 720 }, //to change default viewport
    args: ["--proxy-server=167.99.16.93:3128"]
  });
  const page = await browser.newPage();
  await page.setRequestInterception(true);
  page.on("request", request => {
    if (["image", "font", "stylesheet"].includes(request.resourceType())) {
      request.abort();
    } else {
      request.continue();
    }
  });
  await page.goto("https://httpbin.org/ip");

  // await page.authenticate({ username: "admin", password: "123456" });
  // await page.goto("https://httpbin.org/basic-auth/admin/123456");
})().catch(e => console.log("error mfuker", e));

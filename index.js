const fs = require("fs");

const request = require("request-promise");
const axios = require("axios");
const cheerio = require("cheerio"); //to get info from full html content

//browser automation/headless browser: using browser with code: puppeteer(google-chrome)
//nightmareJs(based on Electron browser)

const urls = [
  {
    url: "https://www.imdb.com/title/tt0411008",
    id: "lost"
  },
  {
    url: "https://www.imdb.com/title/tt5912064/",
    id: "kims_convenience"
  }
];

(async () => {
  let movieData = [];
  for (let movie of urls) {
    const response = await axios(movie.url, {
      headers: {
        Authority: "www.imdb.com",
        Method: "GET",
        Path: "/title/tt5912064/",
        SCHEME: "https",
        Accept:
          "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
        "Accept-Encoding": "gzip, deflate, br",
        "Accept-Language": "tr-TR,tr;q=0.9,en-US;q=0.8,en;q=0.7",
        "Cache-Control": "max-age=0",
        "Upgrade-Insecure-Requests": 1,
        "User-Agent":
          " Mozilla/5.0 (iPad; CPU OS 11_0 like Mac OS X) AppleWebKit/604.1.34 (KHTML, like Gecko) Version/11.0 Mobile/15A5341f Safari/604.1"
      }
    });

    let $ = cheerio.load(response.data);

    //selector or select element-> copy >copy element
    let title = $("div[class='title_wrapper'] > h1")
      .text()
      .trim();
    let rating = $("span[itemprop='ratingValue']").text();
    let posterImg = $("div[class='poster'] > a > img").attr("src");
    let genres = [];
    $("div[class='subtext'] > a[href^='/search/title?genres=']").each(function(
      i,
      el
    ) {
      genres.push($(this).text());
    });

    movieData.push([title, rating, posterImg, genres]);

    // to get a file/img we need a stream
    let file = fs.createWriteStream(`${movie.id}.jpg`);

    axios({
      method: "get",
      url: posterImg,
      responseType: "stream",
      headers: {
        Method: "GET",
        SCHEME: "https",
        Accept:
          "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
        "Accept-Encoding": "gzip, deflate, br",
        "Accept-Language": "tr-TR,tr;q=0.9,en-US;q=0.8,en;q=0.7",
        "Cache-Control": "max-age=0",
        "Upgrade-Insecure-Requests": 1,
        "User-Agent":
          "Mozilla/5.0 (iPad; CPU OS 11_0 like Mac OS X) AppleWebKit/604.1.34 (KHTML, like Gecko) Version/11.0 Mobile/15A5341f Safari/604.1"
      }
    })
      .then(function(response) {
        response.data.pipe(file);
      })
      .catch(e => console.log("something went wrong", e))
      .finally(() => console.log("img downloaded"));
  }

  // to get a file/img from a site, we need a stream

  // csv
  // const Json2csvParser = require("json2csv").Parser;
  // const parser = new Json2csvParser();
  // const csv = parser.parse(movieData);
  // fs.appendFileSync("./data.csv", csv, "utf8");
})();

const puppeteer = require("puppeteer");
const amazon = require("./amazon");

(async () => {
  await amazon.init();

  let prod1 = await amazon.getProductDetails(
    "https://www.amazon.com/HyperX-Cloud-Core-Gaming-Headset/dp/B0153XL4V2/"
  );

  console.log(prod1);

  let prod2 = await amazon.getProductDetails(
    "https://www.amazon.com/dp/B0757DVF4Z/ref=s9_acsd_bw_wf_a_ECDeskto_cdl_1"
  );

  console.log(prod2);

  await amazon.close();
})().catch(e => console.log(e));

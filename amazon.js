const puppeteer = require("puppeteer");

let browser = null;
let page = null;

//urls
const BASE_URL = "https://amazon.com/";

const amazon = {
  init: async () => {
    console.log("Starting Amazon scrapper");

    browser = await puppeteer.launch({ headless: false });
    page = await browser.newPage();
    await page.goto(BASE_URL);
    await page.waitFor(2000);

    // page.on("console", msg => {
    //   console.log(`from browser console: ${msg.text()}`);
    // });

    console.log("Initialization compeleted");
  },
  getProductDetails: async link => {
    console.log(`Going to the page ${link}`);

    await page.goto(link);
    await page.waitFor(3000);

    console.log("parsing details");

    let details = await page.evaluate(() => {
      let title = document.querySelector("#productTitle").innerText;
      let manufacturer = document.querySelector("#bylineInfo").innerText;
      let star = document.querySelector("#acrPopover").getAttribute("title");
      let totalCustomerReviews = document.querySelector(
        "#acrCustomerReviewText"
      ).innerText;
      let price = document.querySelector(
        "#priceblock_ourprice, #priceblock_dealprice"
      ).innerText;

      return {
        title,
        manufacturer,
        price,
        star,
        totalCustomerReviews
      };
    });

    console.log("details parsed");
    return details;
  },
  close: async () => {
    console.log("Closing the scrapper");
    await browser.close();
  }
};

module.exports = amazon;

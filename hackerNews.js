const Nightmare = require("nightmare");
require("nightmare-inline-download")(Nightmare);

//nightmare-inline-download for downloading files
//require("nightmare-inline-download")(Nightmare)
//adds a .download(path) function
//let nightmare = Nightmare();
//nightmare.goto(url).click("selector").download("path/to")

//await nightmare.goto(url)
//await nightamer.click(selector)
//let download = nigthmare.download(path);
//console.log(download) //has info about download

//injecting a js file to page
// await nightmare.goto(url)
// await nightmare.inject("js", "./path/of/file")

let nightmare = null;

const ycombinator = {
  init: async () => {
    nightmare = Nightmare({
      show: false
      //openDevTools: { mode: "detach" }
    });
  },
  getArticles: async (limit = 30) => {
    await nightmare.goto("https://news.ycombinator.com/");

    let articles = [];
    let isPagination = null;
    do {
      let data = await nightmare.evaluate(() => {
        let tempArts = [];

        let tableRows = document.querySelectorAll(
          "table[class='itemlist'] tbody > tr"
        );

        for (let row of tableRows) {
          if (row.getAttribute("class") == "spacer") continue;
          if (row.getAttribute("class") == "athing") {
            let title = row.querySelector(".title a").innerText;
            let url = row.querySelector(".title a").getAttribute("href");
            let source = row.querySelector(".sitestr")
              ? row.querySelector(".sitestr").innerText
              : "no link";

            let nextRow = row.nextElementSibling;

            let points = nextRow.querySelector("span.score")
              ? nextRow.querySelector("span.score").innerText
              : false;
            let author = nextRow.querySelector("a.hnuser")
              ? nextRow.querySelector("a.hnuser").innerText
              : false;
            let date = nextRow.querySelector("span.age").innerText;
            let comments = nextRow.querySelectorAll("a")[3]
              ? nextRow.querySelectorAll("a")[3].innerText
              : false;

            tempArts.push({
              title,
              url,
              source,
              points,
              author,
              date,
              comments
            });
          }
        }
        return tempArts;
      });

      articles = [...articles, ...data];

      isPagination = await nightmare.exists("a.morelink");

      if (isPagination && articles.length < limit) {
        await nightmare.click("a.morelink");
        await nightmare.wait("table[class='itemlist']");
      }
    } while (articles.length < limit && isPagination);

    return articles.slice(0, limit);
  },
  close: async () => {
    await nightmare.end();
  }
};

module.exports = ycombinator;

const puppeteer = require("puppeteer");
//emulate a mobile device
const devices = require("puppeteer/DeviceDescriptors");

(async () => {
  const browser = await puppeteer.launch({
    headless: false
  });
  const page = await browser.newPage();
  //emulate a mobile device
  await page.emulate(devices["iPhone 6"]);
  await page.goto("https://google.com");

  //must be headless
  // await page.pdf({ path: "./page.pdf", format: "A4" });

  //works if it is not headless, headless is false
  // let title = await page.title();
  // let url = await page.url();
  //console.log(title, url);

  // await page.type(".gLFyf.gsfi", "hmm", { delay: 100 });
  // await page.keyboard.press("Enter");

  // await page.waitForNavigation();

  // await page.screenshot({ path: "example.png" });
  //await browser.close();
  debugger;
})().catch(e => console.log("aaa", e));
